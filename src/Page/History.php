<?php

namespace Lesstif\Confluence\Page;


class History
{
    /** @var  boolean */
    public $latest;

    /** @var  array */
    public $createdBy;

    /** @var string */
    public $createdDate;
    
    /** @var array */
    public $lastUpdated;

    /** @var  array */
    public $_links;
    
    /** @var  array */
    public $_expandable;
}